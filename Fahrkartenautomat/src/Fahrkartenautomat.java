﻿import java.util.Scanner;

class Fahrkartenautomat
{
	static Scanner tastatur = new Scanner(System.in);
	 
    public static void main(String[] args)
    {
    
    	while(true) {
    		int zuZahlenderBetrag = fahrkartenbestellungErfassen();
    		int rückgabewert = fahrkartenBezahlen(zuZahlenderBetrag);
    		fahrkartenAusgeben();
    		rueckgeldAusgeben(rückgabewert);
    		System.out.print("\n\n\n");
    	}
    	
    	//tastatur.close();
    }
    
  
 
    public static int fahrkartenbestellungErfassen() {
    	 
    	int zuZahlenderBetrag = 0;
    	int anzahlTickets = 0;
    	int befehl = -1;
    	
    	
    	double[] listePreise = {2.90, 3.30, 360, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    	String[] listeNamen = {"Einzelfahrschein Berlin AB ", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC",
    							"Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC",
    							"Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
    							"Kleingruppen-Tageskarte Berlin ABC"};
    	
    	System.out.print("Wählen Sie ihrer Tarife für Berlin AB aus:\r\n");
    	
    	for(int i = 0; i < listePreise.length; i++) {
    		System.out.printf("\t(%d) %s [%.2f€]\n", (i+1), listeNamen[i], listePreise[i]);
    	}
    	
    	System.out.print("  \nIhre Wahl: ");
    	
    	while(befehl < 1 || befehl >10) {
    		befehl = Character.getNumericValue(tastatur.next().charAt(0));
    		if(befehl < 1 || befehl >10) {
    			System.out.print("\nUngültige Eingabe\nIhre Wahl: ");
    		}
    	}

    	zuZahlenderBetrag = (int) (listePreise[befehl-1]*100);
    	
    	while(anzahlTickets < 1 || anzahlTickets > 10) {
        	System.out.print("Anzahl dieser Ticketart: ");
        	anzahlTickets = tastatur.nextInt();
        	if(anzahlTickets < 1 || anzahlTickets > 10) {
        		System.out.println("Bitte geben Sie einen Wert zwischen 1 und 10 ein.");
        	}
         }
         
         zuZahlenderBetrag = zuZahlenderBetrag * anzahlTickets;
         return zuZahlenderBetrag;
    	
    }
    
    public static int fahrkartenBezahlen(int zuZahlenderBetrag) {
    	int eingezahlterGesamtbetrag = 0;
    	int eingeworfeneMünze;
    	
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f\n", (((double)(zuZahlenderBetrag - eingezahlterGesamtbetrag))/100));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = (int) (tastatur.nextDouble() * 100);
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void warte(int pause) {
    	try {
 			Thread.sleep(pause);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
    }
    
    public static void rueckgeldAusgeben( int rückgabebetrag) {
         if(rückgabebetrag > 0)
         {
      	 //FA-02: durch %.2f wird der double mit zwei Nachkommastellen ausgegeben
      	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", (((double)rückgabebetrag)/100));
      	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           muenzeAusgeben(rückgabebetrag);
         }

         System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                            "vor Fahrtantritt entwerten zu lassen!\n"+
                            "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
    public static void muenzeAusgeben(int rückgabebetrag){
    	while(rückgabebetrag >= 200) // 2 EURO-Münzen
        {
     	  System.out.println("2 EURO");
	          rückgabebetrag -= 200;
        }
        while(rückgabebetrag >= 100) // 1 EURO-Münzen
        {
     	  System.out.println("1 EURO");
	          rückgabebetrag -= 100;
        }
        while(rückgabebetrag >= 50) // 50 CENT-Münzen
        {
     	  System.out.println("50 CENT");
	          rückgabebetrag -= 50;
        }
        while(rückgabebetrag >= 20) // 20 CENT-Münzen
        {
     	  System.out.println("20 CENT");
	          rückgabebetrag -= 20;
        }
        while(rückgabebetrag >= 10) // 10 CENT-Münzen
        {
     	  System.out.println("10 CENT");
	          rückgabebetrag -= 10;
        }
        while(rückgabebetrag >= 5)// 5 CENT-Münzen
        {
     	  System.out.println("5 CENT");
	          rückgabebetrag -= 5;
        }
    }
    
}